#!/usr/bin/env python

import sys
import argparse
import requests

def main():

    parser = argparse.ArgumentParser(description='Send message to username/chat')
    parser.add_argument('token', help='Bot token, like 123456:35randomchars (see https://core.telegram.org/bots for telegram bot introduction). Token can be specified as file:/path/to/token/file.txt, where first line of file.txt is bot token')
    parser.add_argument('chat_id', help='Chat ID. Can be obtained by sending message to bot and checking inbox with checkincoming.py script')
    parser.add_argument('message', help='Message to send. \'-\' (single dash) means "read message from stdin"')
    parser.add_argument('-c', '--continuous', help=u'Continous mode (only with message == "-") - read line by line from stdin and send each line as message - usefull for tailf log monitoring, syslog backend and so on', action='store_true')


    args = parser.parse_args()

    token = args.token
    if token.startswith('file:'):
        token = open(token.partition(':')[2]).readline().strip()[:200]
    chat_id = args.chat_id
    message = args.message

    if message == '-':
        if args.continuous:
            while True:
                message = sys.stdin.readline().strip()

                if message:
                    r = requests.post('https://api.telegram.org/bot%s/sendMessage' % (token),
                                      params={
                                          'chat_id': chat_id,
                                          'text': message,
                                      })


                    if not r.ok:
                        print("Error sending message. Status code: %s, content:" % r.status_code)
                        print(r.content)
                        sys.exit(1)
                else:
                    break
            return
        else:
            message = sys.stdin.read()


    r = requests.post('https://api.telegram.org/bot%s/sendMessage' % (token),
                      params={
                          'chat_id': chat_id,
                          'text': message,
                      })


    if not r.ok:
        print("Error sending message. Status code: %s, content:" % r.status_code)
        print(r.content)
        sys.exit(1)



if __name__ == '__main__':
    main()
