#!/usr/bin/env python3

import argparse
import requests
import json
import time
from datetime import datetime

import redis


class Worker(object):


    def log(self, message, *args):

        if not hasattr(self, '_log_last'):
            self._log_last = time.time()

        msg = message
        if args:
            try:
                msg = message % args
            except Exception as ex:
                print("Error logging message, check it: message = %s, args = %s" % (repr(message), repr(args)))
                print(ex)

        if True:
            print(u"%s (+%.6fs) %s" % (datetime.today().strftime('%Y-%m-%d %H:%M:%S'), time.time() - self._log_last, msg))

        self._log_last = time.time()


    def main(self):
        parser = argparse.ArgumentParser(description="""Push redis queue to telegram.
Redis queue should consists of JSON hashes.
Required fields:
  * token - bot token
  * chat_id - target chat
  * message - message text
  * parse_mode - enum("Markdown", "HTML")
Optional fields:
  * message_id - if present, edit this message ID (editMessageText call)
""")
        parser.add_argument('db_id', help='Redis DB id')
        parser.add_argument('queue', help=u'Redis queue name')


        args = parser.parse_args()

        self.log("Listening to queue %s at db %s", args.queue, args.db_id)

        self.rds = redis.Redis(host='localhost', port=6379, db=args.db_id)

        while True:

            qlen = self.rds.llen(args.queue)
            self.log("DB: %s, queue %s length: %d", args.db_id, args.queue, qlen)
            if qlen:

                self.log("Fetching message")
                queue_name, blob = self.rds.blpop(args.queue)
                try:
                    entry = json.loads(blob)
                except ValueError as ex:
                    self.log("Error decoding message: %s", ex)
                    entry = None

                if entry:
                    self.log("Queue entry: %s", entry)
                    self.send(entry)
                    self.log("Sent")
            else:
                time.sleep(1)




    def send(self, entry):

        params = entry.copy()
        del(params['token'])
        if params.get('message'):
            params['text'] = params['message']
            del(params['message'])

        if entry.get('message_id'):

            r = requests.post('https://api.telegram.org/bot%s/editMessageText' % (entry['token']),
                              params=params,
                              timeout=10)

        else:
            r = requests.post('https://api.telegram.org/bot%s/sendMessage' % (entry['token']),
                              params=params,
                              timeout=10)




        if not r.ok:
            self.log("Error sending message. Status code: %s, content:" % r.status_code)
            self.log("Response: %s", repr(r.content))


            return False
        else:

            if r.json()['ok']:
                return r.json()['result']['message_id']




        return False





if __name__ == '__main__':
    worker = Worker()
    worker.main()
