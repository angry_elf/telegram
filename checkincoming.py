#!/usr/bin/env python

import argparse

import requests

def main():
    parser = argparse.ArgumentParser(description='Check bot\'s incoming messages')
    parser.add_argument('token', help='Bot token, like 123456:35randomchars (see https://core.telegram.org/bots for telegram bot introduction). Token can be specified as file:/path/to/token/file.txt, where first line of file.txt is bot token')
    parser.add_argument('--silent', help=u'Do not yell on empty inbox (for use in cron tasks)', action='store_true')
    parser.add_argument('--show-all', help=u'Show all updates, not only messages (verbose mode)', action='store_true')

    args = parser.parse_args()

    token = args.token
    if token.startswith('file:'):
        token = open(token.partition(':')[2]).readline().strip()[:200]


    r = requests.post('https://api.telegram.org/bot%s/getUpdates' % (token))
    if not r.ok:
        print("Error loading incoming messages, code: %d, response: %s" % (r.status_code, r.content))


    response = r.json()

    update_id = None
    if response['result']:
        for item in response['result']:
            if update_id is None or item['update_id'] > update_id:
                update_id = item['update_id']

            if 'message' in item:
                print("Received message from %s (chat_id: %s): \"%s\"" % (item['message']['from'].get('username', ''), item['message']['chat']['id'], item['message'].get('text')))

            elif args.show_all:
                print("Received update %s" % item)
    else:
        if not args.silent:
            print("No incoming messages")


    if update_id:
        print("Marking messages as read...")
        r = requests.post('https://api.telegram.org/bot%s/getUpdates' % (token),
                          params={'offset': update_id + 1})

        if r.ok and r.json()['ok']:
            print('Done')
        else:
            print("Error occured, code: %d, response: %s" % (r.status_code, r.content))




if __name__ == '__main__':
    main()
