Telegram Sendmessage
====================

Telegram is instant messaging service with wide platform supported.

Telegram also had full-featured API for creating bots. Unfortunatelly, I didn't found any simple program for just sending messages (like, for cron task reports, monitoring notifications).


Usage
=====


sendmessage.py script
---------------------

```
./sendmessage.py -h
usage: sendmessage.py [-h] [-c] token chat_id message

Send message to username/chat

positional arguments:
  token             Bot token, like 123456:35randomchars (see
                    https://core.telegram.org/bots for telegram bot
                    introduction). Token can be specified as
                    file:/path/to/token/file.txt, where first line of file.txt
                    is bot token
  chat_id           Chat ID. Can be obtained by sending message to bot and
                    checking inbox with checkincoming.py script
  message           Message to send. '-' (single dash) means "read message
                    from stdin"

optional arguments:
  -h, --help        show this help message and exit
  -c, --continuous  Continous mode (only with message == "-") - read line by
                    line from stdin and send each line as message - usefull
                    for tailf log monitoring, syslog backend and so on
```


checkincoming.py script
-----------------------

```
./checkincoming.py -h
usage: checkincoming.py [-h] [--silent] token

Check bot's incoming messages

positional arguments:
  token       Bot token, like 123456:35randomchars (see
              https://core.telegram.org/bots for telegram bot introduction).
              Token can be specified as file:/path/to/token/file.txt, where
              first line of file.txt is bot token

optional arguments:
  -h, --help  show this help message and exit
  --silent    Do not yell on empty inbox (for use in cron tasks)
```


How to deal with it
===================


1. Get telegram client for any platform, start and activate it
2. Create bot by messaging to @BotFather, get token
3. Send message to your bot
4. Install scripts in your favorite location (```cd /opt && git clone https://angry_elf@bitbucket.org/angry_elf/telegram.git```, for example)
5. Check incoming messages to bot with ```/opt/telegram/checkincoming.py TOKEN``` command, save chat_id from output
6. Send messages to this chat_id with ```/opt/telegram/sendmessage.py TOKEN CHAT_ID MESSAGE``` command. 


Hint
----

You can send multi-line, broken charsets, impossible characters by passing them to stdin of ```sendmessage.py``` script, like that:

```
cat /etc/fstab | sendmessage.py TOKEN CHAT_ID - 
```
